package sequencia;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Interesse {
    
    private String nomeInteresse;
    private int idInteresse;
    private Usuario user;
    
    public int inserirInteresse() {

        Conexao c = new Conexao();

        Connection dbConnection = c.getConexao();
        PreparedStatement ps = null;
        Statement st = null;
        int idInteresse = 0;

        String insertTableSQL = "INSERT INTO OO_Interesse"  + "(idInteresse, idUsuario, nomeInteresse) VALUES" + "(interesse_seq.nextval,?,?)";

        try {

            String generatedColumns[] = {"idInteresse"};

            ps = dbConnection.prepareStatement(insertTableSQL, generatedColumns);

            st = dbConnection.createStatement();
               
            ps.setInt(1, user.getIdUsuario());
            ps.setString(2, nomeInteresse);

            //execute insert SQL statement
            ps.executeUpdate();

            //ver qual codigo da tua venda
            System.out.println("Record is inserted into OO_Interesse table!");

            ResultSet rs = ps.getGeneratedKeys();
            int chaveGerada = 0;

            while (rs.next()) {
                chaveGerada = rs.getInt(1);
                System.out.println("id gerado: " + chaveGerada);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idInteresse;
    }

    public Usuario getUser() {
        return user;
    }

    public void setUser(Usuario user) {
        this.user = user;
    }
    
    public int getIdInteresse() {
        return idInteresse;
    }

    public void setIdInteresse(int idInteresse) {
        this.idInteresse = idInteresse;
    }


    public String getNomeInteresse() {
        return nomeInteresse;
    }

    public void setNomeInteresse(String nomeInteresse) {
        this.nomeInteresse = nomeInteresse;
    }
    
}

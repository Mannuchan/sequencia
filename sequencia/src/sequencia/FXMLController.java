package sequencia;

import java.awt.event.ActionEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class FXMLController implements Initializable {

    @FXML
    private Label aviso;
    @FXML
    private TextField nomeTextField;
    @FXML
    private PasswordField senhaPasswordField;
    @FXML
    private PasswordField reSenhaPasswordField;
    @FXML
    private TextArea interessesTextArea;    

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    @FXML
    private void cadastra() {  
        if(!(nomeTextField.getText().isEmpty() || senhaPasswordField.getText().isEmpty() || reSenhaPasswordField.getText().isEmpty()  || interessesTextArea.getText().isEmpty())){
            if(!(this.verificaSenhaIgual(senhaPasswordField.getText(), reSenhaPasswordField.getText()))){
         aviso.setText("Digite uma senha igual nos dois campos...");
     }else{
            aviso.setText("Cadastro");
            Usuario l=new Usuario();
            l.setNome(nomeTextField.getText());
            l.setSenha(senhaPasswordField.getText());
            l.inserirUsuario(l);
            
            String[]texto=interessesTextArea.getText().split(",");
            
            for(int c=0;c<texto.length;c++){
                Interesse inte = new Interesse();
                
                inte.setUser(l);
                inte.setNomeInteresse(texto[c]);
                inte.inserirInteresse();
                
            }     
                aviso.setText("Cadastrado com sucesso");
                nomeTextField.clear();
                senhaPasswordField.clear();
                reSenhaPasswordField.clear();
        }    
    }else{
        aviso.setText("Preencha todos os campos...");
        }
    }
    
      private boolean verificaSenhaIgual(String senha1, String senha2){
     if(senha1.equals(senha2)) 
          return true;
     return false;
 }

}
